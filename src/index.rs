//! Search index.

use serde::{Deserialize, Serialize};

use std::fs::File;
use std::path::PathBuf;

use crate::projection;
use chrono;
use std::io::{BufReader, BufWriter, Write};

#[derive(Serialize, Deserialize, Debug)]
pub struct Index {
    pub created: String,
}

impl Index {
    pub fn new() -> Index {
        Index {
            created: chrono::offset::Utc::now().to_string(),
        }
    }
    pub fn load(db_filepath: &PathBuf) -> Index {
        match File::open(db_filepath) {
            Err(_) => panic!("Can't load index!"),
            Ok(file) => {
                let reader = BufReader::new(file);
                match serde_json::from_reader(reader) {
                    Err(_) => panic!("Can't parse Index file"),
                    Ok(result) => {
                        return result;
                    }
                }
            }
        }
    }

    pub fn save(&mut self, db_filepath: &PathBuf) {
        eprintln!("Saving Index file...");

        match File::create(db_filepath) {
            Err(message) => {
                eprintln!("Can't open Index file to save, skipping: {}.", message);
            }
            Ok(file) => {
                eprintln!("Writing Index file...");
                let mut writer = BufWriter::new(file);
                match serde_json::to_writer_pretty(&mut writer, &self) {
                    Err(message) => {
                        eprintln!("Can't save index: {}", message);
                    }
                    Ok(_result) => {
                        eprintln!("Saved index OK.");
                    }
                }
                writer.flush();

                eprintln!("Finished index...");
            }
        }
    }
}

pub fn build(projection_db: &projection::Database) -> Index {
    let index = Index::new();

    // TODO build index here.

    // Demo stub.
    let mut count: u32 = 0;
    for (_id, ref _abc) in projection_db.entries.iter() {
        count += 1;
    }

    eprintln!("Indexed {} docs.", count);
    index
}
