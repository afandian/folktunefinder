//! Structure that represents projections of individual ABC documents.

use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fs::File;
use std::path::PathBuf;

use crate::abc_lexer::Lexer;
use crate::{abc_database, tune_ast};
use std::io::{BufReader, BufWriter, Write};

use rayon::prelude::*;
use std::sync::mpsc::{Receiver, SyncSender};
use crate::features;
use crate::text;
use crate::pitch;



// No need to include the AST in this - by the time we've projected it's no longer used.
// For a 200,000 file tunedb the ABC text weighs 86MB and the JSON serialized AST is 2.1 GB.
#[derive(Serialize, Deserialize, Debug)]
pub struct Projection {
    id: u32,
    abc: String,
    features: Vec<(String, String)>,
    titles: Vec<String>,
    pitches: pitch::PitchSequence,
    intervals: pitch::IntervalSequence,
    interval_histogram: pitch::IntervalHistogram,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Database {
    pub entries: HashMap<u32, Projection>,
}

impl Database {
    pub fn new() -> Database {
        let database = Database {
            entries: HashMap::new(),
        };

        database
    }

    pub fn load(db_filepath: &PathBuf) -> Database {
        let mut database = Database::new();

        match File::open(db_filepath) {
            Err(_) => println!("Can't open initial Projection Database, starting a new one."),
            Ok(file) => {
                let reader = BufReader::new(file);
                match serde_json::from_reader(reader) {
                    Err(_) => panic!("Can't parse Projection Database"),
                    Ok(result) => {
                        database.entries = result;
                    }
                }
            }
        }

        database
    }

    pub fn save(&mut self, db_filepath: &PathBuf) {
        eprintln!("Saving Projection Database...");

        match File::create(db_filepath) {
            Err(message) => {
                eprintln!(
                    "Can't open Projection Database to save, skipping: {}.",
                    message
                );
            }
            Ok(file) => {
                eprintln!("Writing Projection Database...");
                // Pretty-printing this takes the file from 300 MB to 700 MB. It's not worth it.
                let mut writer = BufWriter::with_capacity(100, file);
                match serde_json::to_writer(&mut writer, &self.entries) {
                    Err(message) => {
                        eprintln!("Error saving ABC Database...");
                        eprintln!("Can't write ABC Database: {}", message);
                    }
                    Ok(_result) => {
                        eprintln!("Saved ABC Database...");
                        eprintln!("Saved ABC Database OK.");
                    }
                }
                writer.flush();
                eprintln!("Finished saving ABC Database...");
            }
        }
    }
}

fn project(id: u32, abc: &String) -> Projection {
    let abc = abc.to_string();

    let content: Vec<_> = abc.chars().collect();
    let ast = tune_ast::read_from_lexer(Lexer::new(&content));

    let features = features::extract_all_features(&ast);
    let titles = text::get_titles(&ast);
    let pitches = pitch::PitchSequence::from_ast(&ast);
    let intervals = pitch::IntervalSequence::from_pitch_sequence(&pitches);
    let interval_histogram = pitch::IntervalHistogram::from_interval_seq(&intervals);

    Projection {
        id,
        titles,
        abc,
        features,
        pitches,
        intervals,
        interval_histogram
    }
}

pub fn derive(abc_db: &abc_database::Database) -> Database {
    let _count = 0;

    let (send, receive): (SyncSender<Projection>, Receiver<Projection>) =
        std::sync::mpsc::sync_channel(rayon::current_num_threads());

    let insert_thread = std::thread::spawn(|| {
        let mut database = Database::new();
        for projection in receive.into_iter() {
            database.entries.insert(projection.id, projection);
        }

        database
    });

    let projections = abc_db
        .entries
        .iter()
        .par_bridge()
        .map(|(id, ref abc)| project(*id, abc));

    let send = projections.try_for_each_with(send, |s, projection| s.send(projection));

    if let Err(e) = send {
        panic!("Unable to send internal data: {:?}", e);
    }

    match insert_thread.join() {
        Ok(db) => db,
        Err(_e) => panic!("Couldn't build db."),
    }
}
