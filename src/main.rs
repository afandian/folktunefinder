use std::env;
use std::path::PathBuf;

mod abc_database;
mod abc_lexer;
mod index;
mod music;
mod projection;
mod tune_ast;
mod features;
mod text;
mod pitch;

extern crate clap;
use clap::{App, Arg};

fn get_base() -> PathBuf {
    let key = "BASE";
    match env::var(key) {
        Ok(base) => {
            let mut path = PathBuf::new();
            path.push(base);
            path
        }
        _ => panic!("Please supply BASE."),
    }
}

pub fn get_abc_db_path() -> PathBuf {
    let mut path = get_base();
    path.push("abc.json");
    path
}

pub fn get_projection_db_path() -> PathBuf {
    let mut path = get_base();
    path.push("projection.json");
    path
}

pub fn get_index_path() -> PathBuf {
    let mut path = get_base();
    path.push("index.json");
    path
}

// Scan the ABC directory and rebuild the ABC Database.
fn scan_db() {
    eprintln!("Scan ABC directory...");
    let db_path = get_abc_db_path();
    let _base = get_base();
    let mut database = abc_database::open_or_create(&db_path);
    database.scan(&get_base());
    database.save(&db_path);
    eprintln!("Finished scanning ABC directory!")
}

fn run_projection() {
    eprintln!("Run projections...");
    let db_path = get_abc_db_path();
    let projection_db_path = get_projection_db_path();
    let abc_db = abc_database::open_or_create(&db_path);
    let mut projection_db = projection::derive(&abc_db);
    projection_db.save(&projection_db_path);
    eprintln!("Finished running projections!")
}

fn build_index() {
    eprintln!("Building index...");
    let projection_db_path = get_projection_db_path();
    let index_path = get_index_path();
    let projection_db = projection::Database::load(&projection_db_path);
    let mut index = index::build(&projection_db);
    index.save(&index_path);
    eprintln!("Finished building index!")
}

fn show_index_info() {
    eprintln!("Loading index...");
    let index_path = get_index_path();
    let index = index::Index::load(&index_path);
    eprintln!("Index was built at: {}", index.created);
    eprintln!("Finished loading index!")
}

fn main() {
    let mut app = App::new("FolkTuneFinder Retournepierre")
        .version("1.0")
        .author("Joe Wass. <joe@afandian.com>")
        .about("Indexing and searching ABC format tunes, serve folktunefinder.com")
        .usage("This runs as a multi-step process, from scanning a directory of numbered ABC files,\
               to extracting data, indexing, clustering and then running a server. Each stage loads\
               from, and saves to, a file. They are split out to help with research and development\
               , and to allow for incremental rebuilds. To run a live server end to end from an ABC\
                directory simply use the options -spicbq\n\
                The stages run in order:\n\
                 - scan\n\
                 - project\n\
                 - index\n\
                 - cluster\n\
                 - build\n\
                 - query")
        .arg(Arg::with_name("scan")
            .short("s")
            .long("scan")
            .multiple(true)
            .help("Scan the ABC Tune directory for new tunes, adding to the abc.json file."))
        .arg(Arg::with_name("project")
            .short("p")
            .long("project")
            .multiple(true)
            .help("Analyze the abc.json file and create projection.json file."))
        .arg(Arg::with_name("index")
            .short("i")
            .long("index")
            .multiple(true)
            .help("Build index from the projection file and create index.json file."))
        .arg(Arg::with_name("cluster")
            .short("c")
            .long("cluster")
            .multiple(true)
            .help("Cluster tunes using the index.json file. This is an optional step, and without clusters the tunes will simply be individual. Can take a long time."))
        .arg(Arg::with_name("build")
            .short("b")
            .long("build")
            .multiple(true)
            .help("Build snapshot.json file from index.json and cluster.json files. This file contains all the data to run a server."))
        .arg(Arg::with_name("query")
            .short("q")
            .long("query")
            .multiple(true)
            .help("Run query server using the snapshot.json file."));

    let matches = app.clone().get_matches();

    let mut something = false;

    if matches.is_present("scan") {
        scan_db();
        something = true;
    }

    if matches.is_present("project") {
        run_projection();
        something = true;
    }

    if matches.is_present("index") {
        build_index();
        show_index_info();
        something = true;
    }

    if matches.is_present("cluster") {
        println!("STUB Cluster");
        something = true;
    }

    if matches.is_present("build") {
        println!("STUB Build");
        something = true;
    }

    if matches.is_present("query") {
        println!("STUB Query");
        something = true;
    }

    if !something {
        app.print_help();
        eprintln!();
    }
}
