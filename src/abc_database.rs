//! A database that stores ABC tunes as strings in a persistent file.
//! Scans the ABC input directory to build.

use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fs::File;
use std::path::PathBuf;

use std::io::{BufReader, BufWriter, Read, Write};

#[derive(Serialize, Deserialize, Debug)]
pub struct Database {
    pub entries: HashMap<u32, String>,
}

// Given a filename of a source ABC file, return the tune ID.
fn tune_id_from_filename(filepath: &PathBuf) -> Option<u32> {
    if let Some(file_name) = filepath.file_name() {
        if let Some(file_name) = file_name.to_str() {
            if let Some(first) = file_name.split(".").next() {
                if let Ok(val) = first.parse::<u32>() {
                    return Some(val);
                }
            }
        }
    }

    return None;
}

impl Database {
    pub fn load(&mut self, db_filepath: &PathBuf) {
        eprintln!("Load ABC database from {}", db_filepath.to_string_lossy());
        match File::open(db_filepath) {
            Err(_) => println!("Can't open initial ABC Database, starting a new one."),
            Ok(file) => {
                let reader = BufReader::new(file);

                match serde_json::from_reader(reader) {
                    Err(_) => panic!("Can't parse ABC Database"),
                    Ok(result) => {
                        self.entries = result;
                    }
                }
            }
        }
    }

    pub fn save(&mut self, db_filepath: &PathBuf) {
        eprintln!("Saving ABC Database...");

        match File::create(db_filepath) {
            Err(message) => {
                eprintln!("Can't open ABC Database to save, skipping: {}.", message);
            }
            Ok(file) => {
                eprintln!("Writing ABC Database...");
                let mut writer = BufWriter::new(file);
                match serde_json::to_writer_pretty(&mut writer, &self.entries) {
                    Err(message) => {
                        eprintln!("Error saving ABC Database...");
                        eprintln!("Can't write ABC Database: {}", message);
                    }
                    Ok(_result) => {
                        eprintln!("Saved ABC Database...");
                        eprintln!("Saved ABC Database OK.");
                    }
                }
                writer.flush();
                eprintln!("Finished saving ABC Database...");
            }
        }
    }

    pub fn scan(&mut self, abcs_path: &PathBuf) {
        println!("Scanning ABC files...");
        let mut glob_path = abcs_path.clone();
        glob_path.push("**");
        glob_path.push("*");
        glob_path.set_extension("abc");

        let mut num_scanned = 0;
        let mut num_indexed = 0;

        // Iterate and load into cache.
        for entry in glob::glob(glob_path.to_str().expect("Can't create path"))
            .expect("Failed to read glob pattern")
        {
            match entry {
                Ok(filepath) => {
                    if let Some(tune_id) = tune_id_from_filename(&filepath) {
                        // Check our index, only read the file if we haven't got it yet.
                        // The offset cache serves as the canonical index of tune IDs.
                        if !self.entries.contains_key(&tune_id) {
                            let mut content = String::new();

                            let mut f = File::open(filepath).expect("file not found");

                            f.read_to_string(&mut content).expect("Can't read file");

                            self.entries.insert(tune_id, content);
                            num_indexed += 1;
                        }
                    } else {
                        eprintln!("Failed to get tune id for path: {}", filepath.display());
                    }
                }
                Err(e) => eprintln!("Error {:?}", e),
            }

            num_scanned += 1;

            if num_scanned % 10000 == 0 {
                eprintln!("Scanned {} tunes, indexed {}", num_scanned, num_indexed);
            }
        }
    }
}

pub fn open_or_create(db_filepath: &PathBuf) -> Database {
    let mut database = Database {
        entries: HashMap::new(),
    };
    database.load(db_filepath);
    return database;
}
