# (re)tournepierre

Joe Wass, joe@afandian.com

Experimental reboot of the FolkTuneFinder search engine in Rust. The all stages of the pipeline (including the abstract syntax tree if needed), can be serialised to disk using a standard format. This allows for better experimentation and re-runs of specific stages. It also allows distribution of a pre-baked index file to the server for quicker startup time.

**This is not all implemented yet.**

This project will pick bits out of [folktunefinder-abc](https://github.com/afandian/folktunefinder-abc).

This will build and print the help text:

    cargo build --release
    BASE=~/tunedb/ target/release/retournepierre -h

## Config

Environment variables:

 - `BASE` - The directory where it looks for and stores data.
 
## Directory structure:

The only input required is the numbered ABC tunes. The rest will be created.

 - `$BASE/any/path/to/<number>.abc` - Any ABC files with a numerical filename will be treated as a tune with that numerical ID.
 - `$BASE/abc.json` - All ABC tunes in one file. 
 - `$BASE/manual.yaml` - File for manual input on certain tunes.
 - `$BASE/projection.json` - Every ABC tune with all required information derived from it. Enough to build an index.
 - `$BASE/cluster.json` - Optional cluster data, to group similar tunes. 
 - `$BASE/snapshot.json` - A single index, grouping and projection file, enough to ship to an online server and stand-up a self-contained search instance.

## Stages

Each stage corresponds to an entry in the database structure.

The flow is:

              .abc files
                 ↓
                ABC  
                 ↓    
    Manual ⇨  Projection
                ↓    ↓
             Cluster ↓
                ↓    ↓
               Snapshot

The program has help, pass the `-h` option.
